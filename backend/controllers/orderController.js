const mongoose = require("mongoose");

const Order = require("../models/Order");
const Product = require("../models/Product");


// ########### Create Order ########### 
module.exports.createOrder = async (data) => {
  if (data.isAdmin) {
    return Promise.resolve('Only users can create a new Order!');
  }

  const products = data.products || [];
  console.log('Received data.products:', products);

  const newOrder = new Order({
    userId: data.userId,
    products: [],
  });

  let totalAmount = 0; // Initialize totalAmount

  for (const product of products) {
    const findProduct = await Product.findById(product.productId);
    console.log('Found product:', findProduct);

    if (!findProduct) {
      // Handle the case where the product is not found
      console.warn('Product not found:', product);
      continue;
    }

    const addProduct = {
      productName: findProduct.sku,
      productId: findProduct._id,
      quantity: product.quantity,
      subTotal: product.quantity * findProduct.srp,
      srp: findProduct.srp,
      sku: findProduct.sku, // Set the sku field
    };
    newOrder.products.push(addProduct);
    totalAmount += addProduct.subTotal; // Update totalAmount
  }

  newOrder.totalAmount = totalAmount;

  try {
    const savedOrder = await newOrder.save();
    return savedOrder;
  } catch (error) {
    console.error(error);
    return false;
  }
};



// ########### Create Order ########### END *********

// ########### Retrieve all Orders by a user ########### 
module.exports.getCart = (data) => {
  // Check if the user is an admin
  console.log(data.userId);
  if (data.isAdmin) {
    return Order.find().then((result) => {
      return result;
    });
  } else {
    // Retrieve carts based on userId
    return Order.find({ userId: data.userId }).then((result) => {
      return result;
    });
  }
};

module.exports.getAllOrders = (data) => {
  if (data.isAdmin) {
    return Order.find().then(result => {
    return result;  
  });
  };
  let message = Promise.resolve('Only admin users can retrieve all orders!');
  return message.then((value) => {
    return {value};
  });
};


// ########### Retrieve all Orders (Admin Only) ########### END *********

// ########### Retrieve User's Orders (Auth Only) ########### 
module.exports.getSingleOrder = (reqParams) => {
  return Order.findById(reqParams.orderId).then(result => {
    return result;
  });
};
// ########### Retrieve User's Orders (Auth Only) ########### END *********

// ########### Add or Update Order ###########
module.exports.addOrUpdateProduct = async (data) => {
  if (data.isAdmin) {
    return Promise.resolve('Only user can update Order!');
  } 

let existingOrder = await Order.findById(data.orderId);
  if (!existingOrder) {
    existingOrder = new Order({
    userId: data.userId,
    products: []
    });
}

const existingProduct = existingOrder.products.find(product => product.productId === data.productId);
    let findProduct;
    if (existingProduct) {
      existingProduct.quantity = data.quantity;
      findProduct = await Product.findById(existingProduct.productId);
      existingProduct.subTotal = existingProduct.quantity * findProduct.price;
    } else {
    findProduct = await Product.findById(data.productId);
      const addProduct = {
        productName: findProduct.name,
        productId: findProduct._id,
        quantity: data.quantity,
        subTotal: data.quantity * findProduct.price
        };
    existingOrder.products.push(addProduct);
}

existingOrder.totalAmount = existingOrder.products.reduce((total, product) => {
return total + product.subTotal;
}, 0);

await existingOrder.save();
return existingOrder;
};
// ########### Add or Update Order ########### END *********

// ########### Remove Order ###########
module.exports.removeProduct = async (data) => {
  if (data.isAdmin) {
    return Promise.resolve('Only user can update Order!');
}

let existingOrder = await Order.findById(data.orderId);
  if (!existingOrder) {
    return Promise.resolve('Order not found!');
}

  const existingProductIndex = existingOrder.products.findIndex(
  (product) => product.productId === data.productId
  );
    if (existingProductIndex !== -1) {
      existingOrder.products.splice(existingProductIndex, 1);
      existingOrder.totalAmount = existingOrder.products.reduce(
      (total, product) => total + product.subTotal,
      0
      );
    await existingOrder.save();
    return existingOrder;
  } else {
    return Promise.resolve('Product not found in the order!');
  }
};
// ########### Remove Order ########### END *********