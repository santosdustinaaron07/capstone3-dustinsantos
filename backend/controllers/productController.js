// [SECTION] Declaration
const Product = require("../models/Product");
const bcrypt = require('bcrypt');
const auth = require('../auth');

// [SECTION] Functions

// Add a product - checks also if SKU is already in use
module.exports.addProduct = (data) => {
	if (data.isAdmin) {
		return Product.exists ({ sku: data.product.sku})
			.then((exists) => {
				if(exists) {
					return false;
				} else {
					let newProduct = new Product ({
						sku: data.product.sku,
						description: data.product.description,
						origPrice: data.product.origPrice,
						img: data.product.img
					});

					const vatRate = 0.03;
					newProduct.percentVat = newProduct.origPrice * vatRate;
					newProduct.srp = Math.ceil(newProduct.origPrice + newProduct.percentVat);

					return newProduct
						.save()
						.then((product, error) => {
							if(error) {
								return false;
							} else {
								return true;
							}
						});
				}
			})
			.catch((error) => {
				console.error(error);
				return false;
			});
	};
	let message = Promise.resolve('User must be ADMIN to add new product!');
	return message.then((value) => {
		return value;
	});
};

// Retrieve all products
module.exports.getAllProducts = () => {
  return Product.find({})
  	.then((result) => {
  		const reorderedProducts = result.map((result) => {
  		  return {
  		    _id: result._id,
  		    sku: result.sku,
  		    category: result.category,
  		    description: result.description,
  		    isActive: result.isActive,
  		    onSale: result.onSale,
  		    srp: result.srp,
  		    inventory: result.inventory,
  		    img: result.img			
  		  };
  		});

  		return reorderedProducts;
  	});
};

// Retrieve all active products
module.exports.getAllActiveProduct = () => {
	return Product.find({isActive : true})
		.then(result => {
			const reorderedProducts = result.map((result) => {
			  return {
			    _id: result._id,
			    sku: result.sku,
			    description: result.description,
			    isActive: result.isActive,
			   	onSale: result.onSale,
			    srp: result.srp,
			    inventory: result.inventory,
			    img: result.img
			  };
			});

			return reorderedProducts;
		});
};

// Retrieve all on sale products
module.exports.getOnSale = () => {
	return Product.find({onSale: true})
		.then((result) => {
			const reorderedSale = result.map ((result) => {
				return {
					_id: result._id,
					sku: result.sku,
					description: result.description,
					isActive: result.isActive,
					onSale: result.onSale,
					srp: result.srp,
					inventory: result.inventory,
					img: result.img
				};
			});

			return reorderedSale;
		});
};

// Retrieve products less than the client's budget
module.exports.getOnBudget = (maxPrice) => {
	const query = maxPrice ? { srp: { $lt: maxPrice} } : {};

	return Product.find(query)
		.then((result) => {
			const reorderedProducts = result.map((result) => {
						return {
				  			_id: result._id,
				  			sku: result.sku,
				  			category: result.category,
				  			description: result.description,
				  			isActive: result.isActive,
				  			onSale: result.onSale,
				  			srp: result.srp,
				  			inventory: result.inventory,
				  			img: result.img
						};
			});

		return reorderedProducts;

		});
};

// Retrieve a single product
module.exports.getSingleProduct = (reqParams) => {
	return Product.findById(reqParams.productId)
		.then(result => {
			if (result) {
			  const reorderedProduct = {
			    _id: result._id,
			    sku: result.sku,
    			description: result.description,
			    isActive: result.isActive,
			    onSale: result.onSale,
			    srp: result.srp,
			    inventory: result.inventory,
			    img: result.img
				};
			
			return reorderedProduct;
			} else {
			  return null; // Return null if product is not found
			}
	});
};


// Update product detail
module.exports.updateProduct = async (data) => {
  if (data.isAdmin) {
    let updatedProduct = {
      sku: data.product.sku,
      description: data.product.description,
      origPrice: Number(data.product.origPrice), // Convert to number
    };

    const vatRate = 0.03;
    updatedProduct.percentVat = updatedProduct.origPrice * vatRate;

    // Use toFixed() to round the result to 2 decimal places
    updatedProduct.srp = Math.ceil((updatedProduct.origPrice + updatedProduct.percentVat).toFixed(2));

    try {
      const product = await Product.findByIdAndUpdate(data.productId, updatedProduct);
      if (product) {
        // Product successfully updated
        return true;
      } else {
        // Product not found
        return false;
      }
    } catch (error) {
      // Error occurred during update
      console.error(error);
      return false;
    }
  }

  return { value: 'User must be ADMIN to update product!' };
};




// Archive product
module.exports.archiveProduct = async (data) => {
  if (data.isAdmin) {
    const product = await Product.findById(data.productId);
    if (!product) {
      return false;
    }
    product.isActive = !product.isActive;
    await product.save();
    return true;
  } else {
    return { message: 'User must be ADMIN to archive product!' };
  }
};

