// [SECTION] Declarations
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// [SECTION] Imports
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const orderRoute = require("./routes/orderRoute");

const app = express();

// [SECTION] Middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Mongoose DB connection
mongoose.connect("mongodb+srv://santosdustinaaron07:Ayu2LjfBRn3QDOpQ@wdc028-course-booking.fhfpbwj.mongodb.net/capstone3API?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);
mongoose.connection.once("open", () => console.log(`Connected to Database!`));

// [SECTION] Sting routes to include
app.use("/users", userRoute);
app.use("/products", productRoute);
app.use("/orders", orderRoute); 

// [SECTION] Listener
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000} !`));