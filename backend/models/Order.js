const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	userId : {
		type : String,
		required: [true, "UserId is required"]
	},
	products : [
		{
			productId: {
				type: mongoose.Schema.Types.ObjectId,
				ref: 'Product',
				required: [true, "Product ID is required !"]
			},
			sku: {
				type: String,
				required: [true, "Product Name is required !"]
			},
			srp: {
				type: Number,
				required: [true, "Product Price is required !"]
			},
			quantity: {
				type: Number,
				required: [true, "Product Quantity is required !"]
			},
			subTotal: {
        		type: Number,
        		required: [true, "Subtotal is required!"]
        	}
		}
	],
	totalAmount : {
		type: Number,
		required: [true, "Total Amount is required !"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
});




module.exports = mongoose.model("Order", orderSchema);
