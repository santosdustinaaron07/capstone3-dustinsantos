// [SECTION] Declaration
const mongoose = require("mongoose");

// [SECTION] Schema declaration
const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required: [true, "First Name is required !"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required !"]
	},
	email: {
		type: String,
		required: [true, "Email is required !"]
	},
	mobileNo: {
		type: Number,
		required: [true, "Mobile Number is required !"]
	},
	password: {
		type: String,
		required: [true, "Password is required !"]
	},
	isAdmin : {
		type: Boolean,
		default: false
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	carts: [{
		cardId: {
			type: String
		},
		products: [{
			productId: {
				type: String
			},
			sku: {
				type: String
			},
			quantity: {
				type: Number
			},
			subtotal: {
				type: String
			}
		}],
		total: {
			type: Number
		},
		purchasedOn: {
			type: Date,
		},
		checkOut: {
			type: Boolean
		}
	}],
	isActive: {
		type: Boolean,
		default: true
	},
});


// [SECTION] Export
module.exports = mongoose.model("User", userSchema);

