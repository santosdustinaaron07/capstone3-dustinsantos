const express = require("express");
const router = express.Router();
const orderController = require("../controllers/orderController");
const productController = require("../controllers/productController");
const userController = require("../controllers/userController");

const auth = require("../auth");


router.post("/create", auth.verify, async (req, res) => {
  const data = {
    productId: req.params.productId,
    products: req.body.products,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
    userId: auth.decode(req.headers.authorization).id,
  };

  try {
    const resultFromController = await orderController.createOrder(data);
    res.send(resultFromController);
  } catch (error) {
    console.error(error);
    res.status(500).send("Error creating the order.");
  }
});


router.get("/myCart", auth.verify, (req, res) => {
  const data = {
    userId: auth.decode(req.headers.authorization).id,
    isAdmin: false, // Set isAdmin to false for regular users
  };
  orderController.getCart(data).then((resultFromController) => res.send(resultFromController));
});

router.get("/allOrders", auth.verify, (req, res) => {

  const data = {
    isAdmin : auth.decode(req.headers.authorization).isAdmin
  }
  orderController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});



router.get("/:orderId", auth.verify, (req,res) => {
  const data = {
    isAdmin : auth.decode(req.headers.authorization).isAdmin,
    userId : auth.decode(req.headers.authorization).id,
    orderId : req.params.orderId
  };
  
  orderController.getSingleOrder(data).then(resultFromController => res.send(resultFromController));
});

router.put("/:orderId/updateOrder", auth.verify, (req,res) => {
  const data = {
    isAdmin : auth.decode(req.headers.authorization).isAdmin,
    userId : auth.decode(req.headers.authorization).id,
    orderId : req.params.orderId
  };
  
  orderController.getSingleOrder(data).then(resultFromController => res.send(resultFromController));
});

// Add or update a product in the order
router.put('/:orderId/addOrUpdateProduct', auth.verify, (req, res) => {
  const data = {
    orderId: req.params.orderId,
    productId: req.body.productId,
    quantity: req.body.quantity,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
    userId: auth.decode(req.headers.authorization).id
  };
  
  orderController.addOrUpdateProduct(data).then(resultFromController => res.send(resultFromController));
});

// Remove a product from the order
router.patch('/:orderId/removeProduct', auth.verify, (req, res) => {
  const data = {
    orderId: req.params.orderId,
    productId: req.body.productId,
    isAdmin: auth.decode(req.headers.authorization).isAdmin,
    userId: auth.decode(req.headers.authorization).id
  };
  
  orderController.removeProduct(data).then(resultFromController => res.send(resultFromController));
});


module.exports = router;