// [SECTION] Dependencies
const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");

const auth = require("../auth");

// [SECTION] Routes

// User registration route
router.post("/register", (req, res) => {
	userController.registerUser(req.body)
		.then(resultFromController => res.send(resultFromController));
})


// User login/ authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body)
		.then(resultFromController => res.send(resultFromController))
});

// User details retrieval
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.getUser({userId : userData.id})
		.then(resultFromController => res.send(resultFromController));
});

// User profile modification
router.put("/:userId", auth.verify, (req, res) => {
	const userId = req.params.userId;
	const updateData = req.body;

	userController.updateProfile( {userId}, updateData)
		.then(resultFromController => res.send(resultFromController));
})

// Set to admin
router.patch("/:userId/setAdmin", auth.verify, (req,res) => {
	const data = {
		userId : req.params.userId,
		user : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	userController.setUserAsAdmin(data).then(resultFromController => res.send(resultFromController));
});


router.post("/checkout", auth.verify, (req, res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}

	userController.checkout(data).then(resultFromController => res.send(resultFromController));
});

router.post("/checkEmail", (req,res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;