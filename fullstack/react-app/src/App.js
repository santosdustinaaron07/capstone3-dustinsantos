// [SECTION] Imports
import {useState, useEffect} from 'react';

import AppNavbar from './components/AppNavbar';
import AddProduct from './components/AddProduct';
import ProductView from './components/ProductView';
import UpdateProduct from './components/UpdateProduct';
import UsersOrders from './components/UsersOrders';
// import OnBudget from './components/OnBudget';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Product from './pages/Product';
import AdminPage from './pages/AdminPage';
import Error from './pages/Error';
import Profile from './pages/Profile';
import Cart from './pages/Cart';

import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';

import './App.css'; 

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        // User is logged in
        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          })
        }
        // User is logged out
        else {
          setUser({
            id: null,
            isAdmin: null
          })
        }
      })
  }, []);

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/home" element={<Home />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/products" element={<Product />} />
              <Route path="/products/:productId" element={<ProductView />} />
              <Route path="/profile" element={<Profile />} />
              <Route path="/cart" element={<Cart/>} />
              
              {user.isAdmin === true ?
                <>
                <Route path="/admin-page" element={<AdminPage />} />
                <Route path="/add-product" element={<AddProduct />} />
                <Route path="/user-orders" element={<UsersOrders />} />
                <Route path="/update-product/:productId" element={<UpdateProduct />} />
                </>
                :
                <Route path="/*" element={<Error />} />

              }
              <Route path="/*" element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}


export default App;

