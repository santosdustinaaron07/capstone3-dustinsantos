import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Form, Button } from 'react-bootstrap';

export default function AddProduct() {
  const { user } = useContext(UserContext);
  const [sku, setSku] = useState('');
  const [description, setDescription] = useState('');
  const [origPrice, setOrigPrice] = useState('');
  const [img, setImg] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (sku !== '' && description !== '' && origPrice !== '' && img !=='') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [sku, description, origPrice, img]);

  function addProduct(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.isAdmin === true) {
          fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
              sku,
              description,
              origPrice,
              img
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              if (data === true) {

                setSku('');
                setDescription('');
                setOrigPrice('');
                setImg('');

                Swal.fire({
                  title: 'Product Added',
                  icon: 'success',
                  text: 'Product added successfully!',
                });

              } else {
                Swal.fire({
                  title: 'Something went wrong',
                  icon: 'error',
                  text: 'Please try again later.',
                });
              }
            });

        } else {
          Swal.fire({
            title: 'Unauthorized Access',
            icon: 'error',
            text: 'You are not authorized to access this page.',
          });
        }
      });
  }

  if (user.isAdmin === true) {
  return (

    <Form onSubmit={(e) => addProduct(e)} className='m-3 p-3' style={{ backgroundColor:'#FFB800' }}>
    <h1 className='p-3 text-center' style={{ fontFamily: 'Roboto', color: 'white' }}>ADD PRODUCT</h1>
      <Form.Group className="my-3" controlId="sku">
        <Form.Label>SKU</Form.Label>
        <Form.Control
          type="text"
          value={sku}
          onChange={(e) => {
            setSku(e.target.value);
          }}
          placeholder="Enter SKU"
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          value={description}
          onChange={(e) => {
            setDescription(e.target.value);
          }}
          placeholder="Enter Description"
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="origPrice">
        <Form.Label>Original Price</Form.Label>
        <Form.Control
          type="number"
          value={origPrice}
          onChange={(e) => {
            setOrigPrice(e.target.value);
          }}
          placeholder="Enter Original Price"
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="img">
        <Form.Label>Image</Form.Label>
        <Form.Control
          type="text"
          value={img}
          onChange={(e) => {
            setImg(e.target.value);
          }}
          placeholder="Enter Image"
          required
        />
      </Form.Group>

      <div className="text-center p-3">
        {isActive ?
          <Button variant="primary" type="submit" id="submitBtn" className='px-5 py-3' style={{ backgroundColor:'#044469' }}>
            Add Product
          </Button>
          :
          <Button variant="primary" type="button" id="submitBtn" className='px-5 py-3' style={{ backgroundColor:'#044469' }} disabled>
            Add Product
          </Button>
        }
      </div>
      </Form>
    )
};
};