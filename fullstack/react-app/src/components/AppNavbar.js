import { useContext } from 'react';
import { Link, NavLink } from 'react-router-dom';

import UserContext from '../UserContext';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import securitechlogo from '../images/logo.png';

export default function AppNavbar() {
  const { user } = useContext(UserContext);

  const linkStyle = { color: 'white' }; // Style for the links

  return (
    <Navbar 
      className="my-navbar px-5" 
      expand="lg"
      style={{ backgroundColor: '#044469' }}
    >
      <Navbar.Brand as={Link} to="/">
        <img 
          src={securitechlogo} 
          alt="Securitech Solution" 
          style={{ width: 'auto', height: '4rem' }}
        />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
        <Nav>
          {user.isAdmin ? (
            <>
              <Nav.Link as={NavLink} to="/admin-page" style={linkStyle}>ADMIN CONTROL</Nav.Link>
              <Nav.Link as={NavLink} to="/profile" style={linkStyle}>PROFILE</Nav.Link>
              <Nav.Link as={NavLink} to="/logout" style={linkStyle}>LOGOUT</Nav.Link>
            </>
          ) : (
            <>
              {user.id !== null ? (
                <>
                  <Nav.Link as={NavLink} to="/products" style={linkStyle}>PRODUCTS</Nav.Link>
                  <Nav.Link as={NavLink} to="/profile" style={linkStyle}>PROFILE</Nav.Link>
                  <Nav.Link as={NavLink} to="/cart" style={linkStyle}>CART</Nav.Link>
                  <Nav.Link as={NavLink} to="/logout" style={linkStyle}>LOGOUT</Nav.Link>
                </>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/" style={linkStyle}>HOME</Nav.Link>
                  <Nav.Link as={NavLink} to="/login" style={linkStyle}>LOGIN</Nav.Link>
                  <Nav.Link as={NavLink} to="/register" style={linkStyle}>REGISTER</Nav.Link>
                </>
              )}
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
