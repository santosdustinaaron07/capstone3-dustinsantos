// [SECTION] Imports
import React from 'react';
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

// [SECTION] Declaration
const Banner = ({ data, backgroundColor, image }) => {
  const { title, content, destination, label } = data;

  const titleContainerStyle = {
    backgroundColor: backgroundColor,
    padding: '1rem',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    minHeight: '50vh', // Adjust this value to control the height of the title container
  };

  const imageStyle = {
    width: '100%', // Adjust the size of the image if needed
    maxWidth: '400px', // Max width of the image
    alignSelf: 'center',
  };

  const buttonStyle = {
    backgroundColor: '#044469', // Change the button color to #044469
    color: 'white', // Change the text color to white
    borderColor: '#044469', // Change the border color to #044469
  };

  return (
    <Row>
      <Col xs={12}>
        <div style={titleContainerStyle}>
          <h1 style={{ background: backgroundColor, color: 'white' }}>{title}</h1>
          <p>{content}</p>
          <Button variant="primary" as={Link} to={destination} style={buttonStyle}>
            {label}
          </Button>
        </div>
      </Col>
    </Row>
  );
};

export default Banner;
