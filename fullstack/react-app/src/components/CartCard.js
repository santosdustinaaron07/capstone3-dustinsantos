import React from 'react';
import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const CartCard = ({ cartItem }) => {
  const { productId, quantity, sku, srp, subTotal } = cartItem;

  return (
    <div className="my-4 d-flex justify-content-center" >
      <Card style={{ width: '18rem', backgroundColor: '#FFB800' }}>
        <Card.Body className="d-flex flex-column justify-content-center align-items-center">
          <Card.Title className="sku-title px-5 py-2" style={{ color: 'whitesmoke', fontWeight: 'bold', fontSize: '1.5rem', backgroundColor: '#044469' }}>
            {sku}
          </Card.Title>
          <Card.Text className="description-text" style={{ fontSize: '.8rem', textAlign: 'center' }}>
            Quantity: {quantity}
            <br />
            Sub Total: {subTotal}
            <br />
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
};

CartCard.propTypes = {
  cartItem: PropTypes.shape({
    productId: PropTypes.string.isRequired,
    quantity: PropTypes.number.isRequired,
    sku: PropTypes.string.isRequired,
    srp: PropTypes.number.isRequired,
    subTotal: PropTypes.number.isRequired,
  }).isRequired,
};

export default CartCard;
