import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Form, Button } from 'react-bootstrap';

export default function CreateOrder() {
  const { user } = useContext(UserContext);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (name !== '' && description !== '' && price !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price]);

  function addProduct(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.isAdmin === true) {
          fetch(`${process.env.REACT_APP_API_URL}/products/add`, {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify({
              name,
              description,
              price,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);
              if (data === true) {

                setName('');
                setDescription('');
                setPrice('');

                Swal.fire({
                  title: 'Product Added',
                  icon: 'success',
                  text: 'Product added successfully!',
                });

              } else {
                Swal.fire({
                  title: 'Something went wrong',
                  icon: 'error',
                  text: 'Please try again later.',
                });
              }
            });

        } else {
          Swal.fire({
            title: 'Unauthorized Access',
            icon: 'error',
            text: 'You are not authorized to access this page.',
          });
        }
      });
  }

  if (user.isAdmin === true) {
  return (

    <Form onSubmit={(e) => addProduct(e)}>
    <h1>Add NEW Product</h1>
      <Form.Group className="my-3" controlId="name">
        <Form.Label>Product Name</Form.Label>
        <Form.Control
          type="text"
          value={name}
          onChange={(e) => {
            setName(e.target.value);
          }}
          placeholder="Enter Product Name"
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="description">
        <Form.Label>Description</Form.Label>
        <Form.Control
          type="text"
          value={description}
          onChange={(e) => {
            setDescription(e.target.value);
          }}
          placeholder="Enter Description"
          required
        />
      </Form.Group>

      <Form.Group className="mb-3" controlId="price">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="number"
          value={price}
          onChange={(e) => {
            setPrice(e.target.value);
          }}
          placeholder="Enter Price"
          required
        />
      </Form.Group>

      {isActive ?
        <Button variant="primary" type="submit" id="submitBtn">
          Add Product
        </Button>
        :
        <Button variant="primary" type="button" id="submitBtn" disabled>
              Add Product
            </Button>
        }
      </Form>
    )
};
};