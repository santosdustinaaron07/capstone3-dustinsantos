// [SECTION] Imports
import { Card, Button, Row } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

import './FeaturedProductCard.css';

import { Link } from 'react-router-dom';

// [SECTION] Function
export default function FeaturedProductCard({ featuredProductProp }) {
  const { _id, sku, description, srp, img } = featuredProductProp;

  return (
    <Row className="justify-content-center my-4">
      <Card style={{ width: '18rem', backgroundColor: '#FFB800' }}>
        <Card.Img variant="top" src={`${process.env.PUBLIC_URL}/${img}`} />
        <Card.Body>
          <Card.Title className="sku-title" style={{ color: 'whitesmoke', fontWeight: 'bold', fontSize: '1.5rem', backgroundColor: '#044469' }}>{sku}</Card.Title>
          <Card.Text className="description-text" style={{ fontSize: '.8rem', textAlign: 'center' }}>{description}</Card.Text>
          <Card.Text className="price-text" style={{ fontSize: '1.2rem', fontWeight: 'bold', color: '#044469' }}>Php {srp}</Card.Text>
          <div className="button-container py-2">
            <Button variant="primary" className="px-5" as={Link} to={`/products/${_id}`}>Add to cart</Button>
          </div>
        </Card.Body>
      </Card>
    </Row>
  );
}
