// [SECTION] Imports
import { Row } from 'react-bootstrap';

import './Footer.css';

// [SECTION] Functions
export default function Footer() {
	return (
		<Row>
		    <footer className="footer p-5" style={{ backgroundColor: '#044469' }}>
		      <div className="container">
		        <div className="footer-clients">
		          <h3>Our Clients</h3>
		          <ul className="footer-list">
		            <li>SBD UMC</li>
		            <li>Villa Karenina</li>
		            <li>Solace Group of Companies</li>
		            <li>Verene's Restaurant</li>
		            <li>Sadsad-Tamesis Law Firm</li>
		          </ul>
		        </div>
		        <div className="footer-contact">
		          <h3>Contact Us</h3>
		          <p>Lawa Obando Bulacan, Philippines</p>
		          <p>Email: admin@securitechsolutions</p>
		          <p>Phone: 09123123456</p>
		        </div>
		      </div>
		    </footer>
	    </Row>
	)
}