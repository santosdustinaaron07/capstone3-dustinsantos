import { Fragment, useState, useEffect } from 'react';
import { Row, Col, Form, Button } from 'react-bootstrap';
import ProductCard from '../components/ProductCard';
import './OnBudget.css';

export default function Products() {
  const [products, setProducts] = useState([]);
  const [maxPrice, setMaxPrice] = useState('');

  useEffect(() => {
    const fetchProducts = () => {
      fetch(`${process.env.REACT_APP_API_URL}/products/budget?maxPrice=${maxPrice}`)
        .then(response => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then(data => {
          setProducts(data);
        })
        .catch(error => {
          console.error('Error fetching products:', error);
        });
    };

    fetchProducts();
  }, [maxPrice]);

  const handleInputChange = (event) => {
    setMaxPrice(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    // No need to call fetchProducts() here as it is handled by useEffect when maxPrice changes
  };

  return (
    <Fragment>
          <Row className="text-center pt-3" style={{ backgroundColor: '#FFB800' }}>
            <h1 style={{ color: '#F9F9F9', fontSize: '3.5rem' }} className="mt-4">
              BUDGET FRIENDLY DEALS
            </h1>
          </Row>
          <Row className="justify-content-center pb-1 px-5" style={{ backgroundColor: '#044469' }}>
            <Form onSubmit={handleSubmit} > {/* Apply the on-budget-form class */}
              <Form.Group className="my-3" controlId="maxPrice">
                <Form.Label style={{ color: 'white' }}>Enter Maximum Price:</Form.Label>
                <Form.Control
                  type="number"
                  placeholder="Enter maximum price"
                  value={maxPrice}
                  onChange={handleInputChange}
                />
              </Form.Group>
            </Form>
          </Row>
          <Row className="justify-content-center" style={{ backgroundColor: '#FFB800' }}>
            {products.map((product) => (
              <Col key={product._id} xs={12} sm={6} md={4} className="mb-4">
                <ProductCard product={product} />
              </Col>
            ))}
          </Row>
        </Fragment>
  );
}
