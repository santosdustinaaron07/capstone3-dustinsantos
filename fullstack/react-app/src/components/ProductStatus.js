import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Form, Button } from 'react-bootstrap';

export default function UpdateProductStatus() {
  const { user } = useContext(UserContext);
  const { productId } = useParams();

  const [isActive, setIsActive] = useState('');
  const [products, setProducts] = useState();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} product={product} />
				)
			}))
		})
	}, [])

  function updateProductStatus(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.isAdmin === true) {
          const requestBody = {};
          if (isActive) {
            requestBody.isActive = isActive;
          }
          fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
            method: 'patch',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify(requestBody),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data === true) {
                Swal.fire({
                  title: 'Product Disabled',
                  icon: 'success',
                });
              } else {
                Swal.fire({
                  title: 'Something went wrong',
                  icon: 'error',
                  text: 'Please try again.',
                });
              }
            })
            .catch((error) => {
              console.error('Error:', error);
            })
        } else {
          Swal.fire({
            title: 'Unauthorized Access',
            icon: 'error',
            text: 'You are not authorized to access this page.',
          });
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }

  function handleClick() {
    setIsActive(prevState => !prevState);
  }

  if (user.isAdmin === true) {
    return (
      <Form onSubmit={updateProductStatus}>
        {isActive ?
          <Button 
            variant="danger"
            type="submit"
            id="submitBtn"
          >
            Disable
          </Button>
          :
          <Button 
            variant="dark" 
            type="button" 
            id="submitBtn" 
            onClick={handleClick}
          >
            Update Product
          </Button>
        }
      </Form>
    )
  } else {
    return <Navigate to="/unauthorized" />;
  }
}
