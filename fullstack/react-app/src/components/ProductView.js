import { useState, useEffect, useContext } from 'react'; 
import UserContext from '../UserContext';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useParams, useNavigate, Link } from 'react-router-dom';

import './ProductView.css';

export default function ProductView() {

  const { user } = useContext(UserContext);

  const navigate = useNavigate();
  const { productId } = useParams();

  const [sku, setSku] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);
  const [img, setImg] = useState("");

  const updateQuantity = (increment) => {
    if (increment) {
      setQuantity(quantity + 1);
    } else if (quantity > 1) {
      setQuantity(quantity - 1);
    }
  };

  const calculateTotal = () => {
    return price * quantity;
  };

  const orderCreate = (productId) => {
    const subTotal = calculateTotal();
    const totalAmount = subTotal;
    const order = {
      products: [
        {
          productId: productId,
          sku: sku,
          quantity: quantity,
          subTotal: subTotal,
          srp: price,
        }
      ]
    };
    fetch(`${process.env.REACT_APP_API_URL}/orders/create`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(order)
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Uh! Oh! Something went wrong",
            icon: "error",
            text: "Please try again."
          });

        } else {
          Swal.fire({
            title: "Order Confirmed!",
            icon: "success",
            text: "Thank you for purchasing."
          });

          navigate("/products");
          

        }
      });
  };



  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        setSku(data.sku);
        setDescription(data.description);
        setPrice(data.srp);
        setImg(data.img);
      });
  }, [productId]);

  return (
      <Container className="justify-content-center p-5 mt-5">
            <Row>
              <Col lg={{ span: 4 }}>
                <Card.Img variant="top" src={`${process.env.PUBLIC_URL}/${img}`} />
              </Col>
              <Col lg={{ span: 6, offset: 1 }}>
                <Card className="my-card" style={{ backgroundColor: '#FFB800' }}>
                  <Card.Body className="text-center my-card">
                    <Card.Title className="p-3 sku-title" style={{ color: 'whitesmoke', fontWeight: 'bold', fontSize: '1.5rem', backgroundColor: '#044469' }}>{sku}</Card.Title>
                    <Card.Subtitle className="pt-3">Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>PhP {price}</Card.Text>
                    <Row>
                      <Col>
                        <Button variant="outline-secondary" onClick={() => updateQuantity(false)}>-</Button>
                      </Col>
                      <Col>
                        <Card.Text className="quantity-text" style={{ fontSize: '1.2rem' }}>{quantity}</Card.Text>
                      </Col>
                      <Col>
                        <Button variant="outline-secondary" onClick={() => updateQuantity(true)}>+</Button>
                      </Col>
                    </Row>
                    <Card.Subtitle>Total Amount:</Card.Subtitle>
                    <Card.Text className="total-amount-text" style={{ fontSize: '1.8rem' }}>PhP {calculateTotal()}</Card.Text>
                    {
                      user.id !== null && !user.isAdmin ?
                        <Button variant="primary" style={{ backgroundColor: '#044469' }} onClick={() => orderCreate(productId)}>Order Now!</Button>
                        :
                        user.isAdmin && <div>You're an admin dummy!</div> ||
                        <Button className="btn btn-danger" as={Link} to="/login">Log in to Order</Button>
                    }
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          </Container>
    );
}