// [SECTION] Imports
import { Row, Button, Col } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import { useContext } from 'react';

import UserContext from '../UserContext';

import './ShopNowButton.css';

// [SECTION] Function
export default function ShopNowButton() {
  const { user } = useContext(UserContext);

  return (
    <>
      {user.id !== null ? (
        <>
          <Row style={{ backgroundColor: '#FFB800' }}>
            <Col xs={12} md={12} className = "py-5">
              <div className="button-container py-md-3 px-5">
                <Link to ="/products">
                  <Button size="lg" block='true' className="custom-button px-5 py-3">
                    Shop Now!
                  </Button>
                </Link>
              </div>
            </Col>
          </Row>
        </>
      ) : (
        <>
          <Row style={{ backgroundColor: '#FFB800' }}>
            <Col xs={12} md={6} className = "py-5">
              <div className="button-container py-md-3 px-5">
                <Link to ="/products">
                  <Button size="lg" block='true' className="custom-button px-5 py-3">
                    Shop Now!
                  </Button>
                </Link>
              </div>
            </Col>
            <Col xs={12} md={6} className = "py-5">
              <div className="button-container py-md-3 px-5">
                <Link to ="/register">
                  <Button size="lg" block='true' className="custom-button px-5 py-3">
                    Register Now!
                  </Button>
                </Link>
              </div>
            </Col>
          </Row>
        </>
      )}
    </>

  );
}