// [SECTION] Imports
import { Carousel, Row, Col } from 'react-bootstrap';
import slider2 from '../images/slider2.png';
import slider1 from '../images/slider1.png';
import slider3 from '../images/slider3.png';

// [SECTION] Export function
export default function Slider() {
	return(
		<Row>
			<Col>
				<Carousel>
				  <Carousel.Item interval={2000}>
				    <img
				      className="d-block w-100"
				      src={slider2}
				      alt="First slide"
				    />
				  </Carousel.Item>
				  <Carousel.Item interval={2000}>
				    <img
				      className="d-block w-100"
				      src={slider1}
				      alt="Second slide"
				    />
				  </Carousel.Item>
				  <Carousel.Item>
				    <img
				      className="d-block w-100"
				      src={slider3}
				      alt="Third slide"
				    />
				  </Carousel.Item>
				</Carousel>
			</Col>
		</Row>

	)
}