import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useParams, Link, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Form, Button } from 'react-bootstrap';

export default function UpdateProduct() {
  const { user } = useContext(UserContext);
  const { productId } = useParams();

  const navigate = useNavigate();
  const [sku, setSku] = useState('');
  const [description, setDescription] = useState('');
  const [origPrice, setOrigPrice] = useState('');
  const [img, setImg] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (sku || description || origPrice || img) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [sku, description, origPrice, img]);

  function updateProduct(e) {
    e.preventDefault();
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.isAdmin === true) {
          const requestBody = {};
          if (sku) {
            requestBody.sku = sku;
          }
          if (description) {
            requestBody.description = description;
          }
          if (origPrice) {
            requestBody.origPrice = origPrice;
          }
          if (img) {
            requestBody.img = img;
          }
          fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`, {
            method: 'PUT',
            headers: {
              'Content-Type': 'application/json',
              Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify(requestBody),
          })
            .then((res) => res.json())
            .then((data) => {
              console.log(data);

              if (data === true) {
                Swal.fire({
                  title: 'Product Updated',
                  icon: 'success',
                  text: 'Product Updated successfully!',
                });

                navigate("/admin-page");

              } else {
                Swal.fire({
                  title: 'Something went wrong',
                  icon: 'error',
                  text: 'Please try again.',
                });
              }
            })
            .catch((error) => {
              console.error('Error:', error);
            })
            .finally(() => {
              setSku('');
              setDescription('');
              setOrigPrice('');
              setImg('');
            });
        } else {
          Swal.fire({
            title: 'Unauthorized Access',
            icon: 'error',
            text: 'You are not authorized to access this page.',
          });
        }
      })
      .catch((error) => {
        console.error('Error:', error);
      });
  }

  if (user.isAdmin === true) {
    return (

      <Form onSubmit={(e) => updateProduct(e)} className='m-3 p-3' style={{ backgroundColor:'#FFB800' }}>
       <h1 className='p-3 text-center' style={{ fontFamily: 'Roboto', color: 'white' }}>UPDATE PRODUCT DETAILS</h1>
        <Form.Group className="my-3" controlId="sku">
          <Form.Label>SKU</Form.Label>
          <Form.Control
            type="text"
            value={sku}
            onChange={(e) => {
              setSku(e.target.value);
            }}
            placeholder="Update SKU"
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="description">
          <Form.Label>Description</Form.Label>
          <Form.Control
            type="text"
            value={description}
            onChange={(e) => {
              setDescription(e.target.value);
            }}
            placeholder="Update Description"
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="origPrice">
          <Form.Label>Original Price</Form.Label>
          <Form.Control
            type="number"
            value={origPrice}
            onChange={(e) => {
              setOrigPrice(e.target.value);
            }}
            placeholder="Update Original Price"
          />
        </Form.Group>

{/*        <Form.Group className="mb-3" controlId="img">
          <Form.Label>Image</Form.Label>
          <Form.Control
            type="text"
            value={img}
            onChange={(e) => {
              setImg(e.target.value);
            }}
            placeholder="Update Image"
          />
        </Form.Group>
*/}
       <div className="text-center p-3">
        {isActive ?
          <Button variant="primary" type="submit" id="submitBtn" className='px-5 py-3' style={{ backgroundColor:'#044469' }}>
            Update Product
          </Button>
          :
          <Button variant="dark" type="button" id="submitBtn" className='px-5 py-3' style={{ backgroundColor:'#044469' }} disabled>
                Update Product
              </Button>
          }
        </div>
      </Form>
    )
};
};