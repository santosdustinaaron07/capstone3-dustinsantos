import { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

export default function OrderList() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/allOrders`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(response => response.json())
      .then(data => {
        if (Array.isArray(data)) {
          setOrders(data);
          console.log(data);
        } else {
          console.error('Invalid data format for orders:', data);
        }
      })
      .catch(error => {
        console.error('Error fetching orders:', error);
      });
  }, []);

  return (
    <Container className='text-center p-3'>
      <h1 style={{ color: 'white' }} className="p-3">ORDERS</h1>
      <Row>
        {orders && Array.isArray(orders) ? (
          orders.map((order, index) => (
            <Col key={order._id} xs={12} sm={6} md={6} lg={4}>
              <Row className="my-3 p-3" style={{ backgroundColor: '#FFB800' }}>
                <Col>
                  <p style={{ fontFamily: 'Roboto', fontSize: '1rem', backgroundColor: '#044469', color:'white', fontWeight:'bolder' }} className='p-3'>{order._id}</p>
                  <p>Customer ID: {order.userId}</p>
                  <p>SKU: {order.products[0].sku}</p>
                  <p style={{ fontFamily: 'Roboto', fontSize: '1rem' }}>Quantity: {order.products[0].quantity}</p>
                  <p>Purchased on: {order.purchasedOn}</p>
                  <p style={{ fontFamily: 'Roboto', fontSize: '1rem' }}>Order Total: {order.totalAmount}</p>
                </Col>
              </Row>
            </Col>
          ))
        ) : (
          <p>No orders found.</p>
        )}
      </Row>
    </Container>
  );
}
