import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import CartCard from '../components/CartCard';

import './Cart.css';

const CartPage = () => {
  const [cartItems, setCartItems] = useState([]);

  useEffect(() => {
    fetchCartData();
  }, []);

  const fetchCartData = () => {
    const token = localStorage.getItem('token');
    if (token) {
      fetch(`${process.env.REACT_APP_API_URL}/orders/myCart`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((data) => {
          setCartItems(data);
          console.log(data);
        })
        .catch((error) => {
          console.error('Error fetching cart data:', error);
        });
    }
  };

  return (
    <Container>
      <h2 className="text-center p-3 cart-heading pb-5">CHECKOUT</h2>
      {cartItems.length > 0 ? (
        <Row style={{ backgroundColor: '#FFB800' }}>
          {cartItems.map((cartItem) => (
            <Col key={cartItem._id} md={4}>
              <CartCard cartItem={cartItem.products[0]} />
            </Col>
          ))}
        </Row>
      ) : (
        <p>No items in cart.</p>
      )}
    </Container>
  );
};

export default CartPage;
