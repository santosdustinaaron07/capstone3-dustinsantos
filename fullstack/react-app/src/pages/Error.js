// [SECTION] Imports
import React from 'react';
import Banner from '../components/Banner';
import Footer from '../components/Footer';

// [SECTION] Functions
export default function Error() {
  const data = {
    title: "Error 404 - Page not found.",
    content: "The page you are looking for cannot be found.",
    destination: "/",
    label: "Back to Home"
  };

  return (
    <>
      <div style={{ backgroundColor: '#FFB800' }}>
        <Banner data={data} backgroundColor="#FFB800" />
      </div>
      
      <Footer/>
    < />
  );
}
