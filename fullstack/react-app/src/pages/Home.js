import Slider from '../components/Slider';
import FeaturedProducts from './FeaturedProducts';
import ShopNowButton from '../components/ShopNowButton';
import Footer from '../components/Footer';
import OnBudget from '../components/OnBudget';

export default function Home() {

	return (
		<>
		<Slider />
		<FeaturedProducts />
		<ShopNowButton />
		<OnBudget />
		<Footer />
		</>
	)
}

