// [SECTION] Imports
import React, { useState, useEffect, useContext } from 'react';
import { Form, Button, Card, Col, Row } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Footer from '../components/Footer';

// [SECTION] Function
export default function Login() {
  const { user, setUser } = useContext(UserContext);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isActive, setIsActive] = useState(false);

  function authenticate(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (typeof data.access !== 'undefined') {
          localStorage.setItem('token', data.access);
          retrieveUserDetails(data.access);

          Swal.fire({
            title: 'You are now logged in!',
            icon: 'success',
            text: 'Welcome to Securitech Solution!',
          });
        } else {
          Swal.fire({
            title: 'Authentication Failed!',
            icon: 'error',
            text: 'Incorrect Email/Password, Please try again!',
          });
        }
      });
    setEmail('');
    setPassword('');
  }

  const retrieveUserDetails = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
        });
      });
  };

  useEffect(() => {
    if (email !== '' && password !== '') {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return user.id !== null ? (
    user.isAdmin ? (
      <Navigate to="/admin-page" />
    ) : (
      <Navigate to="/home" />
    )
  ) : (
    <>
    <div className="container">
          <Row className="justify-content-center align-items-center" style={{ backgroundColor: '#FFB800' }}>
            {/* Left Side - Image */}
            <Col sm={12} md={6}>
                      <img
                        src="../images/product1.jpeg"
                        alt="Your Image"
                        className="img-fluid"
                        style={{ objectFit: 'cover', width: '100%', height: 'auto' }}
                      />
                    </Col>

            {/* Right Side - Form */}
            <Col sm={12} md={6}>
              <div
                            className="form-container mx-auto p-3"
                            data-aos="zoom-in"
                            style={{ backgroundColor: '#FFB800' }} // Set the form's background color to FFB800
                          >
                <Form onSubmit={(e) => authenticate(e)}>
                  <Form.Group controlId="userEmail" className="my-3">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                      type="email"
                      placeholder="Enter email"
                      value={email}
                      onChange={(e) => setEmail(e.target.value)}
                      required
                    />
                    <Form.Text className="text-muted">
                      We'll never share your email with anyone else.
                    </Form.Text>
                  </Form.Group>

                  <Form.Group controlId="password">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      type="password"
                      placeholder="Password"
                      value={password}
                      onChange={(e) => setPassword(e.target.value)}
                      required
                    />
                  </Form.Group>

                  <div className="d-flex justify-content-center">
                  {isActive ? (
                    <Button variant="primary my-3" type="submit" style={{ backgroundColor: '#044469' }}>
                      Submit
                    </Button>
                  ) : (
                    <Button variant="primary my-3" type="submit" disabled style={{ backgroundColor: '#044469' }}>
                      Submit
                    </Button>
                  )}
                </div>
                </Form>
              </div>
            </Col>
          </Row>
        </div>
        <Footer />
        </>
  );
}
