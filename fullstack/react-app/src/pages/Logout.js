// [SECTION] Import
import {useContext, useEffect} from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

// [SECTION] Function
export default function Logout() {

	const {unsetUser, setUser} = useContext(UserContext);
	unsetUser();

	useEffect(() => {
		setUser({id:null})
	});

	return (
		<Navigate to="/login"/>
	)
}