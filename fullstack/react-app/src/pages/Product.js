import { Fragment, useState, useEffect } from 'react';
import { Row, Col } from 'react-bootstrap';

import Footer from '../components/Footer';
import FeaturedProducts from './FeaturedProducts';
import ProductCard from '../components/ProductCard';
import OnBudget from '../components/OnBudget';

import './FeaturedProduct.css';

export default function Products() {
  const [products, setProducts] = useState();

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/active`)
      .then(res => res.json())
      .then(data => {
        console.log(data);
        setProducts(
          data.map(product => (
            <Col key={product._id} xs={12} sm={6} md={4} className="mb-4">
              <ProductCard product={product} />
            </Col>
          ))
        );
      });
  }, []);

  return (
    <Fragment>
      <Row className="text-center py-3" style={{ backgroundColor: '#FFB800' }}>
        <h1 style={{ color: '#F9F9F9', fontSize: '3.5rem' }} className="my-4">
          BEST OPTIONS
        </h1>
      </Row>
      <Row className="justify-content-center" style={{ backgroundColor: '#FFB800' }}>
        {products}
      </Row>
      <>
      	<FeaturedProducts />
        <OnBudget />
      	<Footer />
      </>
    </Fragment>
  );
}
