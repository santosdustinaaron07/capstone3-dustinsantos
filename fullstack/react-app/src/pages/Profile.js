import React, { useState, useEffect } from 'react';
import { Form, Row, Col, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import Footer from '../components/Footer';

import './Profile.css';

const UserDetailsPage = () => {
  const [userDetails, setUserDetails] = useState(null);
  const [isEditing, setIsEditing] = useState(false);
  const [updatedUserDetails, setUpdatedUserDetails] = useState({});

  useEffect(() => {
    fetchUserDetails();
  }, []);

  const fetchUserDetails = () => {
    const token = localStorage.getItem('token');
    if (token) {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(response => {
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
        return response.json();
      })
      .then(data => {
        setUserDetails(data);
      })
      .catch(error => {
        console.error('Error fetching user details:', error);
      });
    }
  };

  const handleEdit = () => {
    setUpdatedUserDetails({
      firstName: userDetails.firstName,
      lastName: userDetails.lastName,
      email: userDetails.email,
      mobileNo: userDetails.mobileNo,
      createdOn: userDetails.createdOn,
    });
    setIsEditing(true);
  };

  const handleSave = () => {
    const token = localStorage.getItem('token');
    if (token) {
      fetch(`${process.env.REACT_APP_API_URL}/users/${userDetails._id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(updatedUserDetails),
      })
        .then((response) => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json();
        })
        .then((data) => {
          setUserDetails(data);
          setIsEditing(false);
          Swal.fire({
            title: 'Profile Updated!',
            icon: 'info',
            text: 'Your profile has been updated successfully.',
            titleClass: 'swal-title',
          });
        })
        .catch((error) => {
          console.error('Error updating user details:', error);
          Swal.fire({
            title: 'Update Failed',
            icon: 'error',
            text: 'An error occurred while updating your profile. Please try again.',
            titleClass: 'swal-title',
          });
        });
    }
  };

  return (
    <>
      <div className="user-details-page" style={{ backgroundColor: '#FFB800' }}>
        {userDetails ? (
          <Container className="user-details-container">
            <Form className="p-3">
              <h2 className="text-center mb-4 p-4" style={{ color: 'white' }}>USER DETAILS</h2>
              <Row className="mb-3">
                <Form.Group as={Col} controlId="firstName">
                  <Form.Label>First Name:</Form.Label>
                  <Form.Control
                    type="text"
                    value={isEditing ? updatedUserDetails.firstName : userDetails.firstName}
                    readOnly={!isEditing}
                    onChange={(e) =>
                      setUpdatedUserDetails({ ...updatedUserDetails, firstName: e.target.value })
                    }
                  />
                </Form.Group>
                <Form.Group as={Col} controlId="lastName">
                  <Form.Label>Last Name:</Form.Label>
                  <Form.Control
                    type="text"
                    value={isEditing ? updatedUserDetails.lastName : userDetails.lastName}
                    readOnly={!isEditing}
                    onChange={(e) =>
                      setUpdatedUserDetails({ ...updatedUserDetails, lastName: e.target.value })
                    }
                  />
                </Form.Group>
              </Row>
              <Form.Group className="mb-3" controlId="email">
                <Form.Label>Email:</Form.Label>
                <Form.Control
                  type="email"
                  value={isEditing ? updatedUserDetails.email : userDetails.email}
                  readOnly={!isEditing}
                  onChange={(e) =>
                    setUpdatedUserDetails({ ...updatedUserDetails, email: e.target.value })
                  }
                />
              </Form.Group>
              <Form.Group className="mb-3" controlId="mobileNo">
                <Form.Label>Mobile No:</Form.Label>
                <Form.Control
                  type="text"
                  value={isEditing ? updatedUserDetails.mobileNo : userDetails.mobileNo}
                  readOnly={!isEditing}
                  onChange={(e) =>
                    setUpdatedUserDetails({ ...updatedUserDetails, mobileNo: e.target.value })
                  }
                />
              </Form.Group>
              <Form.Group className="mb-5" controlId="createdOn">
                <Form.Label>Created On:</Form.Label>
                <Form.Control
                  type="text"
                  value={userDetails.createdOn}
                  readOnly
                />
              </Form.Group>
              {isEditing ? (
                <div className="text-center">
                  <Button className="mx-3" variant="primary" style={{ backgroundColor: '#044469', color: '#FFF' }} onClick={handleSave}>
                    Save
                  </Button>
                  <Button variant="secondary" onClick={() => setIsEditing(false)}>
                    Cancel
                  </Button>
                </div>
              ) : (
                <div className="text-center my-3">
                  <Button variant="outline-primary" style={{ backgroundColor: '#044469', color: '#FFF' }} onClick={handleEdit}>
                    Edit
                  </Button>
                </div>
              )}
            </Form>
          </Container>
        ) : (
          <p className="text-center">Loading user details...</p>
        )}
      </div>
      <Footer />
    </>
  );
};

export default UserDetailsPage;
